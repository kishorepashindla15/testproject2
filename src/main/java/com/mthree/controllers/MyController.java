package com.mthree.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.mthree.models.Employee;
import com.mthree.models.Product;
import com.mthree.services.ProductService;

@Controller
public class MyController {
	
	@Autowired
	private ProductService productService;
	
	// Request mapping methods
	@RequestMapping(value="/",method=RequestMethod.GET)
	public String greeting() {
		return "welcome";
	}
	
	@RequestMapping(value="/hello",method=RequestMethod.GET)
	public void sayHello(HttpServletResponse response) throws IOException
	{
		PrintWriter pw = response.getWriter();
		
		pw.println("Hello Kishore!!!");
		
	}

	@RequestMapping("/register")
	public String register(@RequestParam("id") int id,@RequestParam("name") String name) {
		return "register";
	}
	

	@RequestMapping("/register2")
	public String register2(@ModelAttribute Employee e) {
		return "register2";
	}
	
	
	@RequestMapping("/getEmployee")
	public ModelAndView getEmployee() {
		Employee e = new Employee();
		e.setId(1);
		e.setName("Kishore");
		ModelAndView mv = new ModelAndView("emp_details","emp",e);
		mv.addObject("date",LocalDate.now());
		return mv;
	}
	

	@RequestMapping("/getEmployee222")
	public ModelAndView getEmployee2() {
		Employee e = new Employee();
		e.setId(1);
		e.setName("Kishore");
		ModelAndView mv = new ModelAndView("emp_details","emp",e);
		mv.addObject("date",LocalDate.now());
		return mv;
	}
	
	@RequestMapping("/addProduct")
	public String addProduct(@ModelAttribute Product p) {
		
		if(productService.addProductProcess(p)!=null)
			return "product_added";
		else
			return "error";
		
	}
	
	
	
	@RequestMapping("/register22")
	public String register2(@RequestParam("id") int id,@RequestParam("name") String name) {
		return "register";
	}
	
	
	

}
