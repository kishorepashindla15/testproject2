package com.mthree.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
//@Table(name="product_details")
public class Product {
	
	@Id // Primary Key Column
	//@GeneratedValue // Auto-increment
	@Column(name="product_id")
	private int productId;
	
	@Column(name="product_title")
	private String productTitle;
	
	@Column(name="product_price")
	private double productPrice;
	

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductTitle() {
		return productTitle;
	}

	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}

	public double getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}

	
	
	
	

}
