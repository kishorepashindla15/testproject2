package com.mthree;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
//@ComponentScan({"com.mthree.repos","com.mthree.service"})
//@EntityScan({"com.mthree.models"})
///@EnableAutoConfiguration
//@Configuration
public class SpringWebMvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringWebMvcApplication.class, args);
	}
	
	
	

}
