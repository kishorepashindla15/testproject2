package com.mthree.repos;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mthree.models.Product;

@Repository
public interface ProductRepository extends CrudRepository<Product, Integer>{
	
	Optional<Product> findProductByProductTitle(String productTitle);
	
	@Query("UPDATE Product p SET p.productTitle =:pTitle WHERE p.productId=:pId")
	int updateProductTitleById(@Param("pTitle") String productTitle,@Param("pId") int productId);
	
	@Query(value="UPDATE product SET product_title = :pTitle WHERE product_id = :pId",nativeQuery=true)
	int updateProductTitleById2(@Param("pTitle") String productTitle,@Param("pId") int productId);

	
	
}
