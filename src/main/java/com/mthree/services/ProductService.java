package com.mthree.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mthree.models.Product;
import com.mthree.repos.ProductRepository;

@Service
public class ProductService {
	
	@Autowired
	private ProductRepository productRepository;
	
	
	
	
	// Business/Service methods
	
	public Product addProductProcess(Product p) {
		
		Product insertedProduct = productRepository.save(p);
		
		if(insertedProduct!=null) {
				return insertedProduct;
		}
		else {
			return null;
		}
		
	}
	
	public Iterable<Product> loadAllProducts(){
		
		return productRepository.findAll();
	}
	
	
	
	
	
	
	

}
