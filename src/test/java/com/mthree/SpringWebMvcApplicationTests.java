package com.mthree;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.mthree.models.Product;
import com.mthree.services.ProductService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringWebMvcApplicationTests {

	
	@Autowired
	private ProductService productService;
	
	Product p;
	
	@Before
	public void setUp()
	{
		// Fixture
		p = new Product();
		p.setProductId(11);
		p.setProductTitle("TV");
		p.setProductPrice(1000);
		
	}
	
	@Test
	public void testProductAdd() {
		
		assertNotNull(productService.addProductProcess(p));
	}
	
	

}
